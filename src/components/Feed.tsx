import React, { useState, useEffect } from "react";
import { fetchPosts } from "./API";
import { PostType } from "../types/types";
import Post from "./Post";

const Feed = () => {
  const [posts, setPosts] = useState<PostType[]>([]);
  const [fetchedPosts, setFetchedPosts] = useState<PostType[]>([]);
  const [numberOfLikes, setNumberOfLikes] = useState(0);
  const [numberOfComments, setNumberOfComments] = useState(0);
  const [loading, setLoading] = useState(false);
  const [refreshingOfPostsNeeded, setRefreshingOfPostsNeeded] = useState(false);

  // We will use a 5s interval to fetch new posts
  useEffect(() => {
    const interval = setInterval(async () => {
      // WE SHOULD ADD A ERROR HANDLING BLOCK HERE
      const fetchedPosts = await fetchPosts(setLoading);

      setFetchedPosts(fetchedPosts);
      if (
        JSON.stringify(posts).length !== JSON.stringify(fetchedPosts).length
      ) {
        setRefreshingOfPostsNeeded(true);
      }
    }, 5000);
    return () => {
      clearInterval(interval);
    };
  }, []);

  return (
    <div className="feed--wrapper">
      <div className="feed--general-info--wrapper">
        <button
          disabled={!refreshingOfPostsNeeded}
          onClick={() => {
            setPosts(fetchedPosts);
            setRefreshingOfPostsNeeded(false);
            let newNumberOfLikes = 0;
            let newNumberOfComments = 0;
            posts.forEach((post) => {
              newNumberOfLikes += post.numberOfPostLikes;
              newNumberOfComments += post.comments.length;
            });
            setNumberOfLikes(newNumberOfLikes);
            setNumberOfComments(newNumberOfComments);
          }}
        >
          Reload
        </button>
        <span>Likes: {numberOfLikes}</span>
        <span>Comments: {numberOfComments}</span>
      </div>
      {!loading &&
        posts?.map((post, index) => {
          return (
            <Post
              key={index}
              title={post.title}
              description={post.description}
              comments={post.comments}
              numberOfPostLikes={post.numberOfPostLikes}
            />
          );
        })}
    </div>
  );
};

export default Feed;
