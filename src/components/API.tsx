import { PostType } from "../types/types";
import { Dispatch, SetStateAction } from "react";

export const fetchPosts = async (
  setLoading: Dispatch<SetStateAction<boolean>>
) => {
  setLoading(true);

  /////////// Set up endpoint for fetching posts

  // const endpoint = ""
  // const posts = await(await fetch(endpoint)).json();

  // Here we mock data so we have some posts to work with

  let posts: PostType[] = [
    {
      title: "Great news!",
      description: "Lorem ipsum is simply...",
      comments: [],
      numberOfPostLikes: 0,
    },
    {
      title: "Another post",
      description: "Contrary to popular belief...",
      comments: [],
      numberOfPostLikes: 0,
    },
  ];

  setLoading(false);

  return posts;
};

export const reloadPosts = () => {};
