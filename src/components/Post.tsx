import React, { useState } from "react";

import Comment from "./Comment";

import { PostType, CommentType } from "../types/types";

const Post: React.FC<PostType> = () => {
  const [title] = useState("");
  const [description] = useState("");
  const [comments, setComments] = useState<CommentType[]>([]);
  const [numberOfPostLikes, setNumberOfPostLikes] = useState(0);
  const [inputComment, setInputComment] = useState("");

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    submitComment(inputComment);
  };

  const submitComment = (inputComment: string) => {
    comments.push({
      text: inputComment,
      datePosted: Date.now().toString(),
    });
    setComments(comments);
  };

  return (
    <div className="post--wrapper">
      <h1>{title}</h1>
      <span>{numberOfPostLikes}</span>
      <button
        onClick={() =>
          // WE SHOULD CHECK IF THE USER ALREADY LIKED THE POST
          // SO WE DON'T HAVE ABUSE OF SYSTEM
          // wE CAN'T DO IT HERE, BECAUSE WE DON'T HAVE ANY WAY
          // TO ID THE USER
          setNumberOfPostLikes(numberOfPostLikes + 1)
        }
      >
        LIKE POST
      </button>
      <p>{description}</p>
      <form onSubmit={(e) => handleSubmit(e)}>
        <input
          type="text"
          value={inputComment}
          onChange={(e) => setInputComment(e.target.value)}
          placeholder="Add your comment here..."
        />
        <input type="submit" />
      </form>
      {comments.map((comment, index) => {
        return (
          <Comment
            key={index}
            text={comment.text}
            datePosted={comment.datePosted}
          />
        );
      })}
    </div>
  );
};

export default Post;
