import React from "react";

import { CommentType } from "../types/types";

const Comment: React.FC<CommentType> = ({ text, datePosted }) => {
  return (
    <div className="comment--wrapper">
      <p>{text}</p>
      <p>{datePosted}</p>
    </div>
  );
};

export default Comment;
