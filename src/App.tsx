import React from "react";
import Feed from "./components/Feed";
import Comments from "./components/Comments";
import { BrowserRouter, Switch, Route, Router } from "react-router-dom";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <header className="App-header">
          <Route path="/Posts" component={Feed} />
          <Route path="/Comments" component={Comments} />
          <Feed />
        </header>
      </div>
    </BrowserRouter>
  );
}

export default App;
