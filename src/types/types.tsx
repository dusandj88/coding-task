export type PostType = {
  title: string;
  description: string;
  comments: CommentType[];
  numberOfPostLikes: number;
};

export type CommentType = {
  text: string;
  datePosted: string;
};
